import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Задание №1
        //Способ switch
        System.out.println("Введите число 1, 2 или 3: ");
        Scanner inputFigure = new Scanner(System.in);
        int i = inputFigure.nextInt();
        System.out.println(i);

        switch (i) {
            case 1 -> System.out.println("Вы ввели число: " + i);
            case 2 -> System.out.println("Вы ввели число: " + i);
            case 3 -> System.out.println("Вы ввели число: " + i);
        }
        //Способ if
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число 1, 2 или 3: ");

        if (sc.hasNextInt()) {
            int number = sc.nextInt();
            System.out.print("Спасибо! Вы ввели число " + number);
        }

        //Задание №2
        //System.out.println("5, 4, 3, 2, 1");


        //Задание №3
        /*int num = 3;
        for(int i = 1; i <= 10; i++) {
            System.out.println(num + "*" + i + "=" + num * i);
        }*/


        //Задание №4
        // Найти среднее значение суммы чисел от 1 до 100
        //101 × 50 = 5050 - сумма чисел
        //5050 : 100 = 50,5 - среднее арифметическое
        //Не понимаю как реализовать


        //Задание №5
       /* int [] array = {5,2,4,8,88,22,10};
        System.out.println("Максимальное число = " + array[4]);*/



    }
}
